<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class about extends CI_Controller {

    
        function __construct()
        {
          parent::__construct();
          //This method will have the credentials validation
         
          $this->load->model('about_model');
        }
	function index()
	{
                $data['title'] = "David W Pyle";
                $data['content'] = $this->about_model->getAboutContents();
                $this->load->view('templates/header',$data);
		$this->load->view('about/about');
                $this->load->view('templates/footer');
	}
        
}

?>
