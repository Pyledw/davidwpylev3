<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {

    
        function __construct()
        {
          parent::__construct();
          //This method will have the credentials validation
          $this->load->helper('form');
          $this->load->model('admin_model');
        }
	function index()
	{
                $data['title'] = "David W Pyle - Admin";
                $data['content'] = $this->admin_model->getAllContents();
                $data['pages'] = $this->admin_model->getAllPages();
                $this->load->view('templates/header',$data);
                $this->load->view('admin/header');
		$this->load->view('admin/home');
                $this->load->view('templates/footer');
	}
        
        function pageContent($pageName)
        {
            
                $data['title'] = "David W Pyle - ". $pageName;
                $data['content'] = $this->admin_model->getPageContents($pageName);
                $data['pages'] = $this->admin_model->getAllPages();
                $data['pageName'] = $pageName;
                $this->load->view('templates/header',$data);
                $this->load->view('admin/header');
		$this->load->view('admin/home');
                $this->load->view('templates/footer');
        }
        
        function editContent($contentID = NULL)
        {
            
                $this->load->library('form_validation');

                $this->form_validation->set_rules('Title', 'Title', 'trim|required|xss_clean|min_length[5]');
                $this->form_validation->set_rules('Content', 'Content', 'trim|min_length[5]|required|xss_clean');
                
                
                if ($this->form_validation->run() == FALSE)
		{
			$data['UPDATED'] = FALSE;
                        
		}
		else
		{
			$this->admin_model->updateContent($contentID);
                        $data["UPDATED"] = TRUE;
		}
                        $data['title'] = "David W Pyle - " ;
                        $data['content'] = $this->admin_model->getContent($contentID);
                        $data['pages'] = $this->admin_model->getAllPages();
                        $this->load->view('templates/header',$data);
                        $this->load->view('admin/header');
                        $this->load->view('admin/editContent');
                        $this->load->view('templates/footer');
        }
        
        function newContent($pageName = NULL)
        {
                $this->load->library('form_validation');

                $this->form_validation->set_rules('Title', 'Title', 'trim|required|xss_clean|min_length[5]');
                $this->form_validation->set_rules('Content', 'Content', 'trim|min_length[5]|required|xss_clean');
                
                
                if ($this->form_validation->run() == FALSE)
		{
			$data['UPDATED'] = FALSE;
                        $data['title'] = "David W Pyle - " ;
                        
                        $data['pages'] = $this->admin_model->getAllPages();
                        $this->load->view('templates/header',$data);
                        $this->load->view('admin/header');
                        $this->load->view('admin/newContent');
                        $this->load->view('templates/footer');
		}
		else
		{
			$contentID = $this->admin_model->newContent($pageName);
                        $data["UPDATED"] = TRUE;
                        $data['title'] = "David W Pyle - " ;
                        $data['content'] = $this->admin_model->getContent($contentID);
                        $data['pages'] = $this->admin_model->getAllPages();
                        $this->load->view('templates/header',$data);
                        $this->load->view('admin/header');
                        $this->load->view('admin/editContent');
                        $this->load->view('templates/footer');
		}
                        
        }
        
        function deleteContent($contentID)
        {
            $this->admin_model->DeleteContent($contentID);
            
            redirect('/admin');
        }
        
}

?>
