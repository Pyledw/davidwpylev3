<?php
Class Portfolio_Model extends CI_Model
{
    function getPortfolioContents()
    {
      $this -> db -> from('Content');
      $this->db->join('PageContent', 'PageContent.ContentID = Content.ContentID');
      $this->db->join('Pages', 'Pages.ID = PageContent.PageID');
      $this->db->where('Pages.ID = 3');
      $this->db->order_by('Position','asc');
      $query = $this -> db -> get();
      
      
      return $query->result();
      
      
    }
}

?>
